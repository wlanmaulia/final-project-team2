<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API-Pro-Web-001_Nama berupa string</name>
   <tag></tag>
   <elementGuidId>301c9bf7-8957-4f71-a3a6-2773db4e9f23</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjI3NWUzNWQ5Yjk1MjgxZWQwM2ZhYzc4YmQ3ZjNjMjEzNGU2Mzc5ZDZhOTRjOGVkZjJiYzhlMWMwZDIzYTVkNDFmZDczNGZjMTZiMWU2NGEiLCJpYXQiOjE2OTg5NDQ5OTAuMjE1ODAyLCJuYmYiOjE2OTg5NDQ5OTAuMjE1ODA1LCJleHAiOjE3MzA1NjczOTAuMjE0MjM1LCJzdWIiOiIxMjA4Iiwic2NvcGVzIjpbXX0.ZLkQX1EnJKUolluJaLq9UURBfWOERdC7VVyJMlVVKrFAe842Zq0DbXA4ekkNlJ2IATk-Oc5-AA_HhWUSOgW4shU-2612hT9XF7QnHbBDFN5381DgvEmVruI1IgrtSSXPNMaHCYsLunkggO6z_9gEojnAo_L0zyWXWi0e0XdZUJ_jT0TKyAKJHp5FuwxNg3aqUERLEAp6BHKtTTf6xle8wEHfzSqnPlVo6oj7s3E0SmMUhGOQudepZ2_RrxyujjzU4BKzVXN8HGZoLs8o0wXUSWXiNkt5O_dKIiuJdvHsBQf4jU6vk7Q-pNWSREcv2ar750-wFSviqLppJkJs82T47KBePcaVBypRVsGkhO3LEY7Bd5tvKQCY6BoaSYdXLDIX-yh6k17ihCRvOmggXYyxt2HRJKPCxVLfAWP5jJx5-zXANp5f_WRBp_i9SWlLDzOvKaqNcKICdFSnFIZ50s_hGMnjQfmv0axZeEpJMz4MIRrJ9dz4NncjjWe3fhFAuNtkj6xWAppaQBfrPMmz9MGq-CFTD1CT1ncrRQFnk1kRy7FJ9SN2L1ihvjaZDXCIqFOXQUu288J5kusM53Rf-DHGB2JPJ6lvErkPNygFYFKOZwyYTjkDqNnuOujweAqoH4q89xDtoiy0nYrBvgf9UOLCIzlSSFpHioyZWUjyDVoTWlI</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;jessica&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;082285974099&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-10-01&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;C:\\Users\\wlan.DESKTOP-33IVI7L\\OneDrive\\Documents\\WEB\\Register web\\Reg-Web-001 (1).JPG&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;Software Dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>9d874db2-2d09-4cdf-ab19-9f1d6a2c7380</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjI3NWUzNWQ5Yjk1MjgxZWQwM2ZhYzc4YmQ3ZjNjMjEzNGU2Mzc5ZDZhOTRjOGVkZjJiYzhlMWMwZDIzYTVkNDFmZDczNGZjMTZiMWU2NGEiLCJpYXQiOjE2OTg5NDQ5OTAuMjE1ODAyLCJuYmYiOjE2OTg5NDQ5OTAuMjE1ODA1LCJleHAiOjE3MzA1NjczOTAuMjE0MjM1LCJzdWIiOiIxMjA4Iiwic2NvcGVzIjpbXX0.ZLkQX1EnJKUolluJaLq9UURBfWOERdC7VVyJMlVVKrFAe842Zq0DbXA4ekkNlJ2IATk-Oc5-AA_HhWUSOgW4shU-2612hT9XF7QnHbBDFN5381DgvEmVruI1IgrtSSXPNMaHCYsLunkggO6z_9gEojnAo_L0zyWXWi0e0XdZUJ_jT0TKyAKJHp5FuwxNg3aqUERLEAp6BHKtTTf6xle8wEHfzSqnPlVo6oj7s3E0SmMUhGOQudepZ2_RrxyujjzU4BKzVXN8HGZoLs8o0wXUSWXiNkt5O_dKIiuJdvHsBQf4jU6vk7Q-pNWSREcv2ar750-wFSviqLppJkJs82T47KBePcaVBypRVsGkhO3LEY7Bd5tvKQCY6BoaSYdXLDIX-yh6k17ihCRvOmggXYyxt2HRJKPCxVLfAWP5jJx5-zXANp5f_WRBp_i9SWlLDzOvKaqNcKICdFSnFIZ50s_hGMnjQfmv0axZeEpJMz4MIRrJ9dz4NncjjWe3fhFAuNtkj6xWAppaQBfrPMmz9MGq-CFTD1CT1ncrRQFnk1kRy7FJ9SN2L1ihvjaZDXCIqFOXQUu288J5kusM53Rf-DHGB2JPJ6lvErkPNygFYFKOZwyYTjkDqNnuOujweAqoH4q89xDtoiy0nYrBvgf9UOLCIzlSSFpHioyZWUjyDVoTWlI</value>
      <webElementGuid>6166e834-d2ec-42da-8d08-17d149c4b348</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.site/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
