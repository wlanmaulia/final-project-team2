<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Too many failed attempts_Passwd</name>
   <tag></tag>
   <elementGuidId>4201495e-e89e-4bdb-b53b-65ba8de736c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;Passwd&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='Passwd']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>49558440-b655-493f-9c15-c4558f81eba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>8c26a4f6-ae78-4841-8602-af8c2eb09a6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>whsOnd zHQkBf</value>
      <webElementGuid>5f491c38-af76-4315-afcb-b2da83b43a9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>jsname</name>
      <type>Main</type>
      <value>YPqjbf</value>
      <webElementGuid>9f348bc7-d9e9-4d38-b5c7-d8a06c6440e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>current-password</value>
      <webElementGuid>12b65766-a79f-45c0-b7a5-939a1a3ea21c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>2807eae4-31e1-4c23-b0ad-68a1722426c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>3dc60a2c-ca7c-43f1-b397-67f15fc22c66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Enter your password</value>
      <webElementGuid>a0eb2a92-ab78-4409-92de-2468932b1114</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Passwd</value>
      <webElementGuid>188cbb4a-4b20-42e5-a765-66ba5f6d313d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>329afc57-8368-49f3-b6dc-14c05192780f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>9f5591a4-101e-43ba-b8bf-7988e00ac099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-initial-dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>bfbdde09-54af-4982-8344-bf2ef57de95a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-initial-value</name>
      <type>Main</type>
      <value>P</value>
      <webElementGuid>2956bf34-bc5c-45f4-a55b-8d5504503ec2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>badinput</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>760dd48f-3eaa-47be-b69e-5593da69662c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)/div[@class=&quot;aCsJod oJeWuf&quot;]/div[@class=&quot;aXBtI Wic03c&quot;]/div[@class=&quot;Xb9hP&quot;]/input[@class=&quot;whsOnd zHQkBf&quot;]</value>
      <webElementGuid>4b7122da-3644-4f1a-9c2b-a4245acbf161</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='Passwd']</value>
      <webElementGuid>2c730f84-f13c-4c88-8535-4f08d040597e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='password']/div/div/div/input</value>
      <webElementGuid>47f7b65e-7ffa-4396-9a05-69d8fe21977e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/input</value>
      <webElementGuid>018a7cd4-4ca8-43b0-83cd-9b66b24793b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @name = 'Passwd']</value>
      <webElementGuid>c623ed34-bb00-48e1-9136-2a336fabdb0f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
