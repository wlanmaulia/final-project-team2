<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify Login Berhasil</name>
   <tag></tag>
   <elementGuidId>9a3ecfc9-d5f7-40fd-8c45-56aedfc466b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>67b01501-0911-41a6-90d9-fd244cfb2936</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>f51d62b8-efff-4e5f-ace1-bcca6dbc0429</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding Bootcamp
            Tech Talent Berkualitas</value>
      <webElementGuid>ae54e92c-33bc-4070-a5a4-aff2c055145d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>a23cb01f-e1c8-4499-99b8-11877033ab47</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>ab8b8f60-037d-4402-a7d3-6cad03e9d186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h1[1]</value>
      <webElementGuid>d258282f-4a5a-4bcd-8a46-356ed0f5c153</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h1[1]</value>
      <webElementGuid>305bdea2-faf5-4e17-89b9-203858cbe78b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding Bootcamp']/parent::*</value>
      <webElementGuid>77dd572e-d17e-4661-949a-8d34ac8f5f26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>910181fd-da7a-47f3-824f-7ce9c671d881</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Coding Bootcamp
            Tech Talent Berkualitas' or . = 'Coding Bootcamp
            Tech Talent Berkualitas')]</value>
      <webElementGuid>4c98dab7-ad44-4c4c-b099-3cdc0b817a44</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
